from pydantic import BaseModel


class CDNprobability(BaseModel):
    local: int = 1
    external: int = 1
