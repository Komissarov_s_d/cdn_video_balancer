from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database.models import Base
from database.models import CDNip, CDNratio
from settings.db import ConfigDatabase

config_db = ConfigDatabase()


def start_db():
    engine = create_engine(config_db.get_path_db(), echo=config_db.DB_DEBUG)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    DBSession = sessionmaker(
        bind=engine,
        autocommit=False,
        autoflush=False,
    )
    session = DBSession()

    if session.query(CDNratio).count() == 0:
        session.add(CDNratio(internal=2, external=4))
        session.commit()

    if session.query(CDNip).count() == 0:
        session.add(CDNip(adress="http://0.0.0.1"))
        session.add(CDNip(adress="http://0.0.0.2"))
        session.add(CDNip(adress="http://0.0.0.3"))
        session.commit()
    session.close()
