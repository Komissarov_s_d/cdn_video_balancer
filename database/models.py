from sqlalchemy import INTEGER, Column, VARCHAR
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class BaseModel(Base):
    __abstract__ = True
    id = Column(INTEGER(), primary_key=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class CDNip(BaseModel):
    __tablename__ = "cdn_ip"
    adress = Column(VARCHAR(2000), nullable=False)


class CDNratio(BaseModel):
    __tablename__ = "cdn_ratio"
    internal = Column(INTEGER(), nullable=False)
    external = Column(INTEGER(), nullable=False)
