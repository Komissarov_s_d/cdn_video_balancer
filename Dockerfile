FROM python:3.10.7
ENV POETRY_VERSION=1.1.14
ENV PYTHONUNBUFFERED=0

WORKDIR /app
COPY ./ /app
RUN pip install -U pip
RUN pip install poetry \
    && poetry config virtualenvs.create false \
    && poetry install

