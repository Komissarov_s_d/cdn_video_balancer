from settings import BaseSettings


class ConfigApi(BaseSettings):
    API_HOST: str = "localhost"
    API_PORT: int = 1111
    API_DEBUG: bool = True
