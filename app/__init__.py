import asyncio

import uvicorn
from sanic import Sanic, response
from sanic.exceptions import SanicException
from sanic_openapi import openapi2_blueprint

import logic.balancer as balancer
import logic.models as logic_models
from settings.app import ConfigApi

api = Sanic(__name__)
config_api = ConfigApi()
api.blueprint(openapi2_blueprint)


def start_api():
    asyncio.run(balancer.set_queue_servers())
    import app.extention

    # what the documentation offers
    # api.run(host=config_api.API_HOST, port=config_api.API_PORT, debug=config_api.API_DEBUG)
    # keeps more stable and greater rps
    # import app.extention
    uvicorn.run(
        api,
        host=config_api.API_HOST,
        port=config_api.API_PORT,
        debug=config_api.API_DEBUG,
    )


@api.route("/")
async def video_cdn(request):
    video_url = request.args.get("video", None)
    if video_url is None:
        raise SanicException(message="not found video url params", status_code=400)
    if video_url.find(".origin-cluster") < 0:
        raise SanicException(message="invalid link", status_code=400)
    url = await balancer.balancer_server_get_url(url=video_url)
    return response.redirect(url, status=301)


@api.route("/test")
async def test(request):
    video_url = request.args.get("video", None)
    if video_url is None:
        raise SanicException(message="not found video url params", status_code=400)
    if video_url.find(".origin-cluster") < 0:
        raise SanicException(message="invalid link", status_code=400)
    url = await balancer.balancer_server_get_url(url=video_url)
    return response.json(body={"ok": 1})


@api.route("/update_ratio")
async def update_ratio(request):
    internal = request.args.get("internal", None)
    external = request.args.get("external", None)
    try:
        session = request.ctx.session
        async with session.begin():
            result = await logic_models.update_ratio(
                session=session, internal=internal, external=external
            )
    except Exception as e:
        return SanicException(message=f"{e}", status_code=500)
    await balancer.set_queue_servers()
    return response.json(result)


@api.route("/add_adress")
async def add_adress(request):
    url = request.args.get("url", None)
    if url is not None:
        try:
            session = request.ctx.session
            async with session.begin():
                result = await logic_models.add_adress(session=session, url=url)
        except Exception as e:
            return SanicException(message=f"{e}", status_code=500)
        await balancer.set_queue_servers()
        return response.json(result)

    else:
        return SanicException(message="url not found", status_code=400)


@api.route("/delete_adress")
async def delete_adress(request):
    id = request.args.get("id", None)
    if id is not None:
        try:
            session = request.ctx.session
            async with session.begin():
                result = await logic_models.del_adress(session=session, id=id)
        except Exception as e:
            return SanicException(message=f"{e}", status_code=500)
        await balancer.set_queue_servers()
        return response.json({"result": result})

    else:
        return SanicException(message="id not found", status_code=400)


@api.route("/get_all_adress")
async def get_all_adress(request):
    session = request.ctx.session
    async with session.begin():
        result = await logic_models.get_adress(session=session)
    return response.json(result)


@api.route("/get_ratio")
async def get_ratio(request):
    session = request.ctx.session
    async with session.begin():
        result = await logic_models.get_ratio(session=session)
    return response.json(result)
