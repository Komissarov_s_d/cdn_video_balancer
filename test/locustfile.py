from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(1, 2)

    @task(1)
    def view_item(self):
        self.client.get(
            f"/test?video=http://s1.origin-cluster/video/1488/xcg2djHckad.m3u8",
            name="/item",
        )
