def get_server_video_url(url: str) -> str:
    if url[:7] == "http://":
        ex_url = url[7:].replace(".origin-cluster", " ").split()
    else:
        ex_url = url[8:].replace(".origin-cluster", " ").split()
    result = "".join(ex_url)
    return result
