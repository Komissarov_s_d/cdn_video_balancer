from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from database.models import CDNip, CDNratio


async def add_adress(url: str, session: AsyncSession) -> dict:
    obj = CDNip(adress=url)
    session.add(obj)
    await session.commit()
    return obj.as_dict()


async def del_adress(id: int, session: AsyncSession) -> bool:
    query = select(CDNip).where(CDNip.id == id)
    obj = await session.execute(query)
    obj = obj.scalar()
    await session.delete(obj)
    await session.commit()
    return True


async def update_ratio(
    session: AsyncSession, internal: int = None, external: int = None
) -> dict:
    query = select(CDNratio).where(CDNratio.id == 1)
    obj = await session.execute(query)
    obj = obj.scalar()
    if internal is not None:
        obj.internal = internal
    if external is not None:
        obj.external = external
    session.add(obj)
    await session.commit()
    return obj.as_dict()


async def get_ratio(session: AsyncSession) -> dict:
    query = select(CDNratio)
    obj = await session.execute(query)
    return obj.scalar().as_dict()


async def get_adress(session: AsyncSession) -> list:
    query = select(CDNip)
    obj = await session.execute(query)
    obj = obj.scalars()
    arr = []
    for i in obj:
        arr.append(i.as_dict())
    return arr
