import random

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from database import config_db, CDNip, CDNratio
from logic.video import get_server_video_url
from models_pydantic import CDNprobability

servers = []
is_redirect_cdn_arr = []
position_cdn_arr = -1
position_server = -1


def get_all_external_cdn(session: Session) -> list[str]:
    arr_sql = session.query(CDNip).all()
    list_result = []
    for i in arr_sql:
        list_result.append(i.adress)
    return list_result


def get_probability(session: Session) -> CDNprobability:
    obj: CDNratio = session.query(CDNratio).one()
    pr = CDNprobability()
    pr.external = obj.external
    pr.local = obj.internal
    return pr


async def set_queue_servers():
    global is_redirect_cdn_arr, servers

    engine = create_engine(config_db.get_path_db(), echo=config_db.DB_DEBUG)
    DBSession = sessionmaker(
        bind=engine,
        autocommit=False,
        autoflush=False,
    )
    session = DBSession()
    probability = get_probability(session=session)
    servers = get_all_external_cdn(session=session)
    session.close()

    is_redirect_cdn_arr = []
    mno = probability.external / probability.local

    if mno >= 1:
        count_local = int(len(servers) / mno)
    else:
        count_local = int(len(servers) / mno)
    if count_local == 0:
        count_local = 1

    for i in servers:
        is_redirect_cdn_arr.append(True)
    for i in range(count_local):
        is_redirect_cdn_arr.append(False)

    random.shuffle(is_redirect_cdn_arr)


async def balancer_server_get_url(url: str) -> str:
    global servers, is_redirect_cdn_arr, position_cdn_arr, position_server

    position_cdn_arr += 1
    if position_cdn_arr >= len(is_redirect_cdn_arr):
        position_cdn_arr = 0

    if is_redirect_cdn_arr[position_cdn_arr]:

        position_server += 1
        if position_server >= len(servers):
            position_server = 0

        url = get_server_video_url(url)
        return f"{servers[position_server]}/{url}"
    else:
        return url
