import database
from app import start_api

if __name__ == "__main__":
    database.start_db()
    start_api()
